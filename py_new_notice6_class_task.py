class First:
    name = 'First'
    type_ = 'CLASS'    
    
    @classmethod
    def get_name(cls):
        return cls.name    
 
    @staticmethod
    def get_type():
        return First.type_


class Second:
    name = 'Second'
    @classmethod
    def get_name(cls):
        return cls.name


class Third:
    @staticmethod
    def get_programming_language():
        return 'python'


class First_hair(First, Second, Third):
    pass


class Second_hair(Third, First, Second):
    @staticmethod
    def get_programming_language():
        return 'PHP'
    # pass


class Third_hair(First, Second, Third):
    pass


print(First.get_name())
print(First.get_type())
print(First_hair.get_name())
print(First_hair.__dict__)
print(Second.get_name())
sec = Second()
print(sec.get_name())
print(Third.get_programming_language())
thi = Third()
print(thi.get_programming_language())
print(Second_hair.__base__.get_programming_language())
print(Second_hair.__dict__)
print(Second_hair.__base__.__dict__)
print(Second_hair.mro())
print(Third_hair.get_type())


